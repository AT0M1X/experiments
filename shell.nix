{ pkgs ? import <nixpkgs> {} }:
with pkgs;

mkShell {
  buildInputs = [
    python38
    poetry
    python38Packages.jupyter
    python38Packages.scipy
  ];
}
